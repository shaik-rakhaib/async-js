# Promises in JavaScript

- Promises in JavaScript is a better way to handle asynchronous operations to not fall into callback hell and inversion of control.

- A Promise is a object representing a pending state of a asynchronous operation.
- A promise can be in one of three states:

    - Pending: Initial state, neither fulfilled nor rejected.
    - Fulfilled: The operation completed successfully, and the promise has a resulting value.
    - Rejected: The operation encountered an error, and the promise has a reason for the error.
```js
const getData=some_api(data);

getData.then((response)=>{

  console.log("Success");

}).
catch((error)=>{
```
### Creating promises
Promise constructor takes two arguments: executor function with resolve and reject functions as parameters.

```js
function fetchData() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const data = 'This is the fetched data';
      const error = false; 
      // Change to true to simulate an error

      if (!error) {
        resolve(data); 
        // Resolve the Promise with the data
      } else {
        reject('Error: Unable to fetch data'); 
        // Reject the Promise with an error message
      }
    }, 2000); // a 2-second delay
  });
}

fetchData()
  .then((data) => {
    console.log('Fetched data:', data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });

```


## Handle Many Promises at Once :

- Some ways to handle many promises at once are as follows,
  - Promise.all()
  - Promise.race()
  - Promise.any()
  - Promise.allSettled()

### The Promise.all() method

- Promise.all() accepts an array of promises as an argument but returns a single promise as the output.
- The single promise it returns resolves with an array of values if all the promises in the input array are fulfilled. 
- The array Promise.all() resolves with will contain the resolve values of individual promises in the input array.

```js
const promise1 = Promise.resolve(`First Promise`);
const promise2 = new Promise((resolve) =>
  setTimeout(resolve, 3000, `Second Promise`)
);
const promise3 = new Promise((resolve) =>
  setTimeout(resolve, 2000, `Third Promise`)
);

Promise.all([promise1, promise2, promise3]);
```

### The Promise.race() method

- Promise.race() accepts an array of promises as an argument and returns a single promise as an output. 
- The single promise it returns is the fastest promise to finish running—resolved or not. 
- This means Promise.race() will return the promise with the shortest execution time in an array of promises

```js
const promise1 = new Promise((resolve) =>
  setTimeout(resolve, 3000, `First Promise's Value`)
);
const promise2 = new Promise((resolve) =>
  setTimeout(resolve, 2000, `Second Promise's Value`)
);
const promise3 = Promise.resolve(`Third Promise's Value`);

Promise.race([promise1, promise2, promise3]);
```

### The Promise.any() method

- Promise.any() accepts an array of Promises as an argument but returns a single Promise as the output. 
- The single promise it returns is the first resolved promise in the input array. 
- This method waits for any promise in the array to be resolved and would immediately return it as the output.

```js
const promise1 = new Promise((resolve) =>
  setTimeout(resolve, 3000, `First Promise`)
);
const promise2 = new Promise((resolve) =>
  setTimeout(resolve, 2000, `Second Promise`)
);
const promise3 = Promise.reject(`Third Promise`);

Promise.any([promise1, promise2, promise3]);
```