# CodeControl in JavaScript

## Synchronous Programming:

- In synchronous code, control flows sequentially from one statement to the next, and you can rely on the order of execution. 
- Error handling is typically done with try-catch blocks to handle exceptions:
  ```js
  console.log("Step 1");

    try {
    // Synchronous operation that may throw an error
    const result = someSynchronousFunction();
    console.log("Step 2");
    } catch (error) {
    console.error("An error occurred:", error);
    }

    console.log("Step 3");
  ```

## Code Control in Asynchronous Programming:

- In asynchronous code, control is not guaranteed to flow sequentially because asynchronous operations can run in the background. 
- Managing control flow and handling errors can be a bit more complex. 
- Here's an example using Promises:
  ```js
  console.log("Step 1");

    someAsynchronousFunction()
    .then((result) => {
        console.log("Step 2");
        return anotherAsynchronousFunction(result);
    })
    .then((finalResult) => {
        console.log("Step 3");
    })
    .catch((error) => {
        console.error("An error occurred:", error);
    });
    
    console.log("Step 4");

  ```

  - In this asynchronous code, you don't have a guaranteed order of execution. 
  - The "Step 2" and "Step 3" may not follow immediately after "Step 1" because they depend on the status of the promises. 
  - Errors are handled using the .catch() method to capture errors in the promise chain. 
  - The "Step 4" may also execute before the promise chain is complete.


### Promises and Callbacks
- Let's use both promises and callbacks to control the flow of asynchronous code. 
- In this example, we have two asynchronous operations, and we'll demonstrate how to use both promises and callbacks to handle them:
  ```js
    // Asynchronous operation 1 with a Promise
    function asyncOperation1() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
        console.log("Async operation 1 completed");
        resolve();
        }, 1000);
    });
    }

    // Asynchronous operation 2 with a callback
    function asyncOperation2(callback) {
    setTimeout(() => {
        console.log("Async operation 2 completed");
        callback();
    }, 1500);
    }

    // Using Promises to control the flow
    asyncOperation1()
    .then(() => {
        asyncOperation2(() => {
        console.log("Both operations completed using Promises and Callbacks.");
        });
    })
    .catch((error) => {
        console.error("An error occurred:", error);
    });

  ```