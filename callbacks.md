# Callbacks in JavaScript.


- Callbacks are the reason asynchronous programming in JS is possible.
- A Callback is a function which is passed as a parameter to an another function.
- They allow us to handle the results of an asynchronous operation in a non-blocking manner.

## Passing Functions as Arguments: 
- In JavaScript, functions are first-class citizens, which means you can pass them as arguments to other functions. 
- Callbacks are essentially functions passed to another function to be executed at a later time.

     ```js
    function fetchData(callback) {
        setTimeout(function () {
            const data = 'some_data';
            callback(null, data); 
        }, 1000);
    }
    function some_function(x,y){
        //do some operation here
    }
    fetchData(some_function);
    ```

- We can use anonymous functions and arrow functions here too.

### With Anonymous Functions :

```js
function fetchData(callback) {
    setTimeout(function () {
        const data = 'some_data';
        callback(null, data); 
    }, 1000);
}
function some_function(x,y){
    //do some operations here
}
fetchData(some_function);
```

### With Arrow functions :
```js
const fetchData = (callback) => {
    setTimeout(()=>{
        const data = 'some_data';
```
